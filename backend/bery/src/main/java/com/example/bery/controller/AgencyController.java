package com.example.bery.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.*;
import com.example.bery.model.Agency;
import com.example.bery.service.AgencyService;

@RestController
@CrossOrigin
@RequestMapping("/agency")
public class AgencyController {
    @Autowired
    AgencyService agencyService;

    //CREATE

    //UPDATE

    //GET (list)
    @GetMapping()
    public ResponseEntity<List<Agency>> getAllAgency() {
        try {
            return new ResponseEntity<>(agencyService.getAllAgency(), HttpStatus.OK);
        } catch(Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    //GET (id)
    @GetMapping("/{id}")
    public ResponseEntity<Agency> getAgencyId(@PathVariable("id") int id) {
        try {
            return new ResponseEntity<>(agencyService.getAgencyId(id), HttpStatus.OK);
        } catch(Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    //DELETE (id)
}
