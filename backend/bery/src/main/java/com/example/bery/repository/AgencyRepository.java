package com.example.bery.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.bery.model.Agency;

public interface AgencyRepository extends JpaRepository<Agency, Integer>{
    
}
