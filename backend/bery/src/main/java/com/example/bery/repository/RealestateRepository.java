package com.example.bery.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.bery.model.Realestate;

public interface RealestateRepository extends JpaRepository<Realestate, Integer>{
    
}
