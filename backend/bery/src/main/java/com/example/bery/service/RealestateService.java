package com.example.bery.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bery.model.Realestate;
import com.example.bery.repository.RealestateRepository;

import java.util.*;

@Service
public class RealestateService {
    @Autowired
    RealestateRepository realestateRepository;

    // CREATE
    public Realestate createRealEstate(Realestate realEstate) {
        Realestate newRealestate = new Realestate();
        if (realEstate != null) {
            BeanUtils.copyProperties(realEstate, newRealestate);
        }
        return realestateRepository.save(newRealestate);
    }

    // UPDATE
    public Realestate updateRealestate(int id, Realestate updateRealEstate) {
        Optional<Realestate> realEstateOptional = realestateRepository.findById(id);
        if (realEstateOptional.isPresent() && updateRealEstate != null) {
            Realestate realestate = realEstateOptional.get();
            BeanUtils.copyProperties(updateRealEstate, realestate, "id");
            Realestate saveRealEstate = realestateRepository.save(realestate);
            return saveRealEstate;
        } else {
            return null;
        }
    }

    // GET (list)
    public List<Realestate> getAllRealEstate() {
        return realestateRepository.findAll();
    }

    // GET (id)
    public Realestate getRealEstateId(int id) {
        Optional<Realestate> realEstateOptional = realestateRepository.findById(id);
        if (realEstateOptional.isPresent()) {
            Realestate realEstateId = realEstateOptional.get();
            return realEstateId;
        } else
            return null;
    }

    // DELETE (id)
    public void deleteRealEstateById(int id) {
        realestateRepository.deleteById(id);
    }
}
