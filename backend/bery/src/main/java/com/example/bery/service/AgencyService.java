package com.example.bery.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bery.model.Agency;
import com.example.bery.repository.AgencyRepository;
import java.util.*;
@Service
public class AgencyService {
    @Autowired
    AgencyRepository agencyRepository;

    //CREATE

    //UPDATE

    //GET(list)
    public List<Agency> getAllAgency() {
        return agencyRepository.findAll();
    }
    //GET(id)
    public Agency getAgencyId(int id) {
        Optional<Agency> agencyOptional = agencyRepository.findById(id);
        if(agencyOptional.isPresent()) {
            Agency agencyId = agencyOptional.get();
            return agencyId;
        }else 
            return null;
    }
    //DELETE(id)
}
