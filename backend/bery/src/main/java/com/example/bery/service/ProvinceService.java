package com.example.bery.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bery.model.Province;
import com.example.bery.repository.ProvinceRepository;
import java.util.*;

@Service
public class ProvinceService {
    @Autowired
    ProvinceRepository provinceRepository;

    // CREATE
    public Province createProvince(Province province) {
        Province newProvince = new Province();
        newProvince.setName(province.getName());
        newProvince.setCode(province.getCode());
        Province saveProvince = provinceRepository.save(newProvince);
        return saveProvince;
    }

    // UPDATE
    public Province updateProvince(int id, Province province) {
        Optional<Province> provinceOptional = provinceRepository.findById(id);

        if (provinceOptional.isPresent()) {
            Province existingProvince = provinceOptional.get();
            existingProvince.setName(province.getName());
            existingProvince.setCode(province.getCode());
            Province updatedProvince = provinceRepository.save(existingProvince);
            return updatedProvince;
        } else {
            return null;
        }
    }

    // GET (List)
    public List<Province> getAllProvince() {
        List<Province> proviceList = provinceRepository.findAll();
        return proviceList;
    }

    // GET (id)
    public Province getProvinceById(int id) {
        Optional<Province> provinceOptional = provinceRepository.findById(id);
        if (provinceOptional.isPresent()) {
            Province provinceId = provinceOptional.get();
            return provinceId;
        } else
            return null;
    }

    // DELETE (id)
    public Province deleteProvinceById(int id) {
        Optional<Province> provinceOptional = provinceRepository.findById(id);
        if (provinceOptional.isPresent()) {
            provinceRepository.deleteById(id);
            return null;
        }
        return null;
    }
}
