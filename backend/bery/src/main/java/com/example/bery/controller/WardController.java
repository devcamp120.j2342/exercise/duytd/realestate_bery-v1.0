package com.example.bery.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.*;

import com.example.bery.model.Ward;
import com.example.bery.service.WardService;

@RestController
@CrossOrigin
@RequestMapping("/ward")
public class WardController {
    @Autowired
    WardService wardService;

    //CREATE

    //UPDATE

    //GET(list)
    @GetMapping()
    public ResponseEntity<List<Ward>> getAllWard() {
        try {
            return new ResponseEntity<>(wardService.getAllWard(), HttpStatus.OK);
        }catch(Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    //GET (id)
    @GetMapping("/{id}")
    public ResponseEntity<Ward> getWardId(@PathVariable("id") int id) {
        try {
            return new ResponseEntity<>(wardService.getWardId(id), HttpStatus.OK);
        } catch(Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    //DELETE(id)
}
