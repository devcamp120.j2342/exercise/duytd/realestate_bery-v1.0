package com.example.bery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BeryApplication {

	public static void main(String[] args) {
		SpringApplication.run(BeryApplication.class, args);
	}

}
