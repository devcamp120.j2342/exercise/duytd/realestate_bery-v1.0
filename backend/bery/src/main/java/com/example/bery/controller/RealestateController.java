package com.example.bery.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.bery.model.Realestate;
import com.example.bery.service.RealestateService;
import java.util.*;

@RestController
@CrossOrigin
@RequestMapping("/realestate")
public class RealestateController {
    @Autowired
    RealestateService realEstateService;

    // CREATE
    @PostMapping()
    public ResponseEntity<Realestate> createRealEstate(@RequestBody Realestate realestate) {
        try {
            return new ResponseEntity<>(realEstateService.createRealEstate(realestate), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // UPDATE
    @PutMapping("/{id}")
    public ResponseEntity<Realestate> updateRealEstateById(@PathVariable("id") int id, Realestate updateRealestate) {
        try {
            return new ResponseEntity<>(realEstateService.updateRealestate(id, updateRealestate), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // GET (list)
    @GetMapping()
    public ResponseEntity<List<Realestate>> getAllRealEstate() {
        try {
            return new ResponseEntity<>(realEstateService.getAllRealEstate(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // GET (id)
    @GetMapping("/{id}")
    public ResponseEntity<Realestate> getRealEstateId(@PathVariable("id") int id) {
        try {
            return new ResponseEntity<>(realEstateService.getRealEstateId(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // DELETE (id)
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteRealestate(@PathVariable("id") int id) {
        try {
            realEstateService.deleteRealEstateById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
