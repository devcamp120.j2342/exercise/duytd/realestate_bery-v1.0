package com.example.bery.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.bery.model.Province;

public interface ProvinceRepository extends JpaRepository<Province, Integer>{
    Province findByName(String name);
}
