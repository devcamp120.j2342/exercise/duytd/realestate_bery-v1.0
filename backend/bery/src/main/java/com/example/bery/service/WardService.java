package com.example.bery.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bery.model.Ward;
import com.example.bery.repository.WardRepository;
import java.util.*;

@Service
public class WardService {
    @Autowired
    WardRepository wardRepository;

    // CREATE

    // UPDATE

    // GET (list)
    public List<Ward> getAllWard() {
        return wardRepository.findAll();
    }

    // GET (id)
    public Ward getWardId(int id) {
        Optional<Ward> wardOptional = wardRepository.findById(id);
        if (wardOptional.isPresent()) {
            Ward wardId = wardOptional.get();
            return wardId;
        } else {
            return null;
        }
    }
    // DELETE (id)
}
