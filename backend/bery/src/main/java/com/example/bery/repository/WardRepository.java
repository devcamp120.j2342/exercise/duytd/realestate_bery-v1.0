package com.example.bery.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.bery.model.Ward;

public interface WardRepository extends JpaRepository<Ward, Integer>{
    
}
