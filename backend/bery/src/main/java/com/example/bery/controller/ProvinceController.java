package com.example.bery.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.*;

import com.example.bery.model.Province;
import com.example.bery.service.ProvinceService;

@RestController
@CrossOrigin
@RequestMapping("/province")
public class ProvinceController {
    @Autowired
    ProvinceService provinceService;

    //CREATE
    @PostMapping()
    public ResponseEntity<Province> createProvince(@RequestBody Province province) {
        try {
            return new ResponseEntity<>(provinceService.createProvince(province), HttpStatus.CREATED);
        } catch(Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //UPDATE
    @PutMapping("/{id}")
    public ResponseEntity<Province> updateProvinceById(@PathVariable("id") int id, @RequestBody Province province) {
        try {
            return new ResponseEntity<>(provinceService.updateProvince(id, province), HttpStatus.OK);
        } catch(Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //GET (list)
    @GetMapping()
    public ResponseEntity<List<Province>> getAllProvince() {
        try {
            return new ResponseEntity<>(provinceService.getAllProvince(), HttpStatus.OK);
        } catch(Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //GET (id)
    @GetMapping("/{id}")
    public ResponseEntity<Province> getProvinceId(@PathVariable("id") int id) {
        try {
            return new ResponseEntity<>(provinceService.getProvinceById(id), HttpStatus.OK);
        } catch(Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //DELETE (id)
    @DeleteMapping("/{id}")
public ResponseEntity<Province> deleteProvinceId(@PathVariable("id") int id) {
    try {
        return new ResponseEntity<>(provinceService.deleteProvinceById(id), HttpStatus.OK);
    } catch(Exception e) {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
}

}