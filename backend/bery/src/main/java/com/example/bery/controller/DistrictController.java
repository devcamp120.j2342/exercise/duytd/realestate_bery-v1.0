package com.example.bery.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.*;

import com.example.bery.model.District;
import com.example.bery.service.DistrictService;

@RestController
@CrossOrigin
@RequestMapping("/district")
public class DistrictController {
    @Autowired
    DistrictService districtService;

    // CREATE
    @PostMapping("/{provinceName}")
    public ResponseEntity<String> addDistrictToProvince(
            @PathVariable String provinceName,
            @RequestBody District newDistrict) {
        try {
            districtService.addDistrictToProvince(provinceName, newDistrict);
            return new ResponseEntity<>("District added to province successfully.", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Error adding district to province.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // UPDATE
    @PutMapping("/{provinceName}/update/{id}")
    public ResponseEntity<String> updateDistrictWithProvince(
            @PathVariable String provinceName,
            @PathVariable int id,
            @RequestBody District updateDistrict) {
        try {
            districtService.updateDistrictById(provinceName, id, updateDistrict);
            return new ResponseEntity<>("District updated with Province successfully.", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Error updating District with Province.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // GET (list)
    @GetMapping()
    public ResponseEntity<List<District>> getAllDistrict() {
        try {
            List<District> districts = districtService.getAllDistrict();
            if (districts.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(districts, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // GET (id)
    @GetMapping("/{id}")
    public ResponseEntity<District> getDistrictById(@PathVariable("id") int id) {
        try {
            District district = districtService.getDistrictId(id);
            if (district == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(district, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // DELETE (id)
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteDistrictbyId(@PathVariable("id") int id) {
        try {
            districtService.DeleteDistrictById(id);
            return new ResponseEntity<>("xóa quận huyện thành công", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
