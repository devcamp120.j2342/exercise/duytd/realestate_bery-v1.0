package com.example.bery.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.bery.model.District;
import com.example.bery.model.Province;
import com.example.bery.repository.DistrictRepository;
import com.example.bery.repository.ProvinceRepository;

import java.util.*;

@Service
public class DistrictService {
    @Autowired
    DistrictRepository districtRepository;

    @Autowired
    ProvinceRepository provinceRepository;

    // CREATE
    public void addDistrictToProvince(String provinceName, District newDistrict) {
        // Tìm province theo tên
        Province existingProvince = provinceRepository.findByName(provinceName);

        if (existingProvince != null) {
            // Set province cho district mới
            newDistrict.setProvince(existingProvince);

            // Lưu district mới vào cơ sở dữ liệu
            districtRepository.save(newDistrict);

            System.out.println("District added to province successfully.");
        } else {
            System.out.println("Province not found.");
        }
    }

    // UPDATE
    public void updateDistrictById(String provinceName, int id, District updateDistrict) {
        // Tìm Province theo tên
        Province existingProvince = provinceRepository.findByName(provinceName);

        // Kiểm tra xem Province có tồn tại không
        if (existingProvince != null) {
            // Tìm District theo ID
            Optional<District> districtOptional = districtRepository.findById(id);

            // Kiểm tra xem District có tồn tại không
            if (districtOptional.isPresent()) {
                District existingDistrict = districtOptional.get();

                // Cập nhật thông tin của District
                existingDistrict.setName(updateDistrict.getName());
                existingDistrict.setPrefix(updateDistrict.getPrefix());

                // Gán District cho Province
                existingDistrict.setProvince(existingProvince);

                // Lưu cập nhật vào cơ sở dữ liệu
                districtRepository.save(existingDistrict);
            }
        }
    }

    // GET (list)
    public List<District> getAllDistrict() {
        List<District> listDistrict = districtRepository.findAll();
        return listDistrict;
    }

    // GET (id)
    public District getDistrictId(int id) {
        Optional<District> districtOptional = districtRepository.findById(id);
        if (districtOptional.isPresent()) {
            District districtId = districtOptional.get();
            return districtId;
        } else
            return null;
    }

    // DELETE (id)
    public void DeleteDistrictById(int id) {
        Optional<District> districtOptional = districtRepository.findById(id);
        if (districtOptional.isPresent()) {
            districtRepository.deleteById(id);
        } else {
            System.out.println("Không tìm thấy quận huyện với id: " + id);
        }
    }
}
