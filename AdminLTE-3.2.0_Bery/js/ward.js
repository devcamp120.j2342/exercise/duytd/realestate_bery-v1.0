// REGION 1
var gTable = $("#example1")
  .DataTable({
    responsive: true,
    lengthChange: false,
    autoWidth: false,
    buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
    columns: [
      {
        data: null,
        render: function (data, type, row) {
          return '<a title="Xem" class="view" style="cursor: pointer;"><i class="fas fa-search"></i></a>';
        },
      },
      {
        data: null,
        render: function (data, type, row) {
          return '<a title="Sửa" class="edit" style="cursor: pointer"><i class="fas fa-edit"></i></a>';
        },
      },
      {
        data: null,
        render: function (data, type, row) {
          return '<a title="Xóa" class="delete" style="cursor: pointer"><i class="fas fa-trash"></i></a>';
        },
      },
      {
        data: null,
        render: function (data, type, row) {
          return '<button class="btn btn-info btn-sm info">Thông tin BDS</button>';
        },
      },
      { data: "id" },
      { data: "name" },
      { data: "prefix" },
      { data: "province.name" },
    ],
  })
  .buttons()
  .container()
  .appendTo("#example1_wrapper .col-md-6:eq(0)");
// REGION 2
onPageLoading();
// Sự kiện click cho nút "Thêm"
$("#btn-add-district").on("click", function () {
  $("#modal-add").modal("show");
});
// Sự kiện click cho nút "add" (modal-add)
$("#btn-modalAdd-save").on("click", function () {
  onBtnModalAddSaveClick();
});
// Sự kiện click cho nút "Xem"
$("#example1").on("click", ".view", function () {
  var vTable = $("#example1").DataTable();
  var rowData = vTable.row($(this).closest("tr")).data();
  $("#modal-view").modal("show");
  $("#view-province-id").html(rowData.id);
  $("#view-district-name").html(rowData.name);
  $("#view-district-prefix").html(rowData.prefix);
  $("#view-district-province").html(rowData.province.name);
});
// Sự kiện click cho nút trên modal-view
$("#modal-view").on("click", ".ew-action", function () {
  var action = $(this).data("action");
  var vId = $("#view-province-id").html();
  var vName = $("#view-district-name").html();
  var vPrefix = $("#view-district-prefix").html();
  var vProvinceName = $("#view-district-province").html();

  if (action === "add") {
    onBtnAddModalViewClick();
  }
  if (action === "edit") {
    onBtnEditModalViewClick(vId, vName, vPrefix, vProvinceName);
  }
  if (action === "delete") {
    onBtnDeleteModalViewClick(vId, vName, vPrefix, vProvinceName);
  }
});
// Sự kiện click cho nút "Sửa"
$("#example1").on("click", ".edit", function () {
  var vTable = $("#example1").DataTable();
  var rowData = vTable.row($(this).closest("tr")).data();
  $("#modal-edit").modal("show");
  $("#inp-edit-idDistrict").val(rowData.id);
  $("#inp-edit-nameDistrict").val(rowData.name);
  // Đặt giá trị cho selectpicker "Quận-Huyện"
  $("#select-edit-prefixDistrict").selectpicker("val", rowData.prefix);
  // Đặt giá trị cho selectpicker "Tên"
  $("#select-edit-provinceDistrict").selectpicker("val", rowData.province.name);
});
// Sự kiện click cho nút save trên modal-edit
$("#btn-modalEdit-save").on("click", function () {
  var districtId = $("#inp-edit-idDistrict").val();
  var vProvinceName = $("#select-edit-provinceDistrict").val();
  callApiPutDistrictById(districtId, vProvinceName);
});
// Sự kiện click cho nút "Xóa"
$("#example1").on("click", ".delete", function () {
  var vTable = $("#example1").DataTable();
  var rowData = vTable.row($(this).closest("tr")).data();
  $("#modal-delete").modal("show");
  $("#el1_province_id").html(rowData.id);
  $("#el1_district__name").html(rowData.name);
  $("#el1_district__prefix").html(rowData.prefix);
  $("#el1_district__province_name").html(rowData.province.name);
});
// Sự kiện click cho nút delete trên modal delete
$("#btn-confirm-modalDelete").on("click", function () {
  var vId = $("#el1_province_id").html();
  callApiDeleteDistrict(vId);
});
// Sự kiện click cho nút "Thông tin bds"
$("#example1").on("click", ".info", function () {
  var vTable = $("#example1").DataTable();
  var rowData = vTable.row($(this).closest("tr")).data();
  console.log("thông tin BDS: " + rowData.id);
});
// Sự kiện hover vào các nút action trên modal view
$('[data-toggle="tooltip"]').tooltip();
// Select 2
$(".selectpicker").selectpicker();
// REGION 3
// Hàm thực hiện tải trang
function onPageLoading() {
  callApiGetAllDistrict();
  callApiGetAllProvince();
}
// Hàm thực hiện sự kiện nút save trên modal add được click
function onBtnModalAddSaveClick() {
  var vDistrictProvince = {
    name: $("#inp-name-district-mda").val().trim(),
    prefix: $("#select-prefix-district-mda").val(),
  };
  var vProvince = $("#select-district-province-mda").val();
  // Kiêm tra dữ liệu đầu vào
  var isCheck = validationModalAdd(vDistrictProvince, vProvince);
  if (isCheck) {
    callApiPostProvince(vDistrictProvince, vProvince);
  }
}
// Hàm thực hiện sự kiện nút add trên modal-view được click
function onBtnAddModalViewClick() {
  $("#modal-view").modal("hide");
  setTimeout(function () {
    $("#modal-add").modal("show");
  }, 250);
}
// Hàm thực hiện sự kiện nút edit trên modal-view được click
function onBtnEditModalViewClick(
  paramId,
  paramName,
  paramPrefix,
  paramProvinceName
) {
  $("#modal-view").modal("hide");
  setTimeout(function () {
    $("#modal-edit").modal("show");
    $("#inp-edit-idDistrict").val(paramId);
    $("#inp-edit-nameDistrict").val(paramName);
    $("#select-edit-prefixDistrict").selectpicker(paramPrefix);
    $("#select-edit-provinceDistrict").selectpicker(paramProvinceName);
  }, 250);
}
// Hàm thực hiện sự kiện nút delete trên modal-view được click
function onBtnDeleteModalViewClick(
  paramId,
  paramName,
  paramPrefix,
  paramProvinceName
) {
  $("#modal-view").modal("hide");
  setTimeout(function () {
    $("#modal-delete").modal("show");
    $("#el1_province_id").html(paramId);
    $("#el1_district__name").html(paramName);
    $("#el1_district__prefix").html(paramPrefix);
    $("#el1_district__province_name").html(paramProvinceName);
  }, 250);
}
// REGION 4
//  Hàm gọi api lấy tất cả danh sách quận huyện
function callApiGetAllDistrict() {
  $.ajax({
    url: "http://localhost:8080/district",
    type: "GET",
    success: function (res) {
      loadDataToTable(res);
    },
    error: function (err) {
      toastr.error("Đã có lỗi xẫy ra");
      console.log(err.status);
    },
  });
}
// Hàm gọi api lấy tất cả danh sách tỉnh thành
function callApiGetAllProvince() {
  $.ajax({
    url: "http://localhost:8080/province",
    type: "GET",
    success: function (res) {
      loadDataToSelectProvinceModalAdd(res);
      loadDataToSelectProvinceModalEdit(res);
    },
    error: function (err) {
      toastr.error("Đã có lỗi xẫy ra");
      console.log(err.status);
    },
  });
}
// Truyền dữ liệu vào trong table
function loadDataToTable(paramData) {
  var vTable = $("#example1").DataTable();
  vTable.clear().rows.add(paramData).draw();
}
// Truyền dữ liệu vào trong select modal add
function loadDataToSelectProvinceModalAdd(provinces) {
  // Làm sạch select trước khi thêm dữ liệu mới
  var vSelectProvince = $("#select-district-province-mda");
  // vSelectProvince.empty();
  // Thêm option vào select
  provinces.forEach(function (province) {
    vSelectProvince.append(
      $("<option>", {
        value: province.name, // Giá trị của option
        html: province.name, // Nội dung hiển thị
      })
    );
  });

  // Khởi tạo lại Bootstrap Selectpicker nếu bạn đang sử dụng
  vSelectProvince.selectpicker("refresh");
}
// Truyền dữ liệu vào trong select province modal edit
function loadDataToSelectProvinceModalEdit(provinces) {
  // Làm sạch select trước khi thêm dữ liệu mới
  var vSelectProvince = $("#select-edit-provinceDistrict");
  // vSelectProvince.empty();
  // Thêm option vào select
  provinces.forEach(function (province) {
    vSelectProvince.append(
      $("<option>", {
        value: province.name, // Giá trị của option
        html: province.name, // Nội dung hiển thị
      })
    );
  });

  // Khởi tạo lại Bootstrap Selectpicker nếu bạn đang sử dụng
  vSelectProvince.selectpicker("refresh");
}
// Hàm thẩm định đầu vào trên modal add
function validationModalAdd(paramObj, paramProvince) {
  if (paramObj.name.trim() === "") {
    // Hiển thị thông báo Toast nếu "name" không được nhập
    toastr.warning("vui lòng nhập tên tỉnh thành");
    return false;
  }
  if (paramObj.prefix === "") {
    toastr.warning("vui lòng chọn quận huyện");
    return false;
  }
  if (paramProvince === "") {
    toastr.warning("vui lòng chọn tên tỉnh thành");
    return false;
  }
  return true;
}
// Hàm gọi api để thêm tỉnh thành
function callApiPostProvince(paramObj, paramProvince) {
  $.ajax({
    url: "http://localhost:8080/district/" + paramProvince,
    type: "POST",
    data: JSON.stringify(paramObj),
    contentType: "application/json",
    success: function (res) {
      toastr.success("Thêm thành công");
      $("#inp-name-district-mda").val("");
      $("#select-prefix-district-mda").val("");
      $("#select-district-province-mda").val("");
      $("#modal-add").modal("hide");
      onPageLoading();
    },
    error: function (err) {
      toastr.error("Đã có lỗi xẫy ra trong quá trình tạo mới");
      console.log(err.status);
    },
  });
}
// Hàm gọi api để update tỉnh thành
function callApiPutDistrictById(paramId, paramProvinceName) {
  var vProvinceObj = {
    name: $("#inp-edit-nameDistrict").val(),
    prefix: $("#select-edit-prefixDistrict").val(),
  };
  $.ajax({
    url: `http://localhost:8080/district/${paramProvinceName}/update/${paramId}`,
    type: "PUT",
    data: JSON.stringify(vProvinceObj),
    contentType: "application/json",
    success: function (res) {
      toastr.success("Cập nhật thành công");
      $("#modal-edit").modal("hide");
      onPageLoading();
    },
    error: function (err) {
      toastr.error("Đã có lỗi xẫy ra trong quá trình cập nhật");
      console.log(err.status);
    },
  });
}
// Hàm gọi api delete tỉnh thành
function callApiDeleteDistrict(paramId) {
  // Hiển thị xác nhận từ người dùng trước khi xóa
  if (confirm("Bạn có chắc chắn muốn xóa quận huyện này không?")) {
    $.ajax({
      url: "http://localhost:8080/district/" + paramId,
      type: "DELETE",
      success: function (res) {
        toastr.success(res);
        $("#modal-delete").modal("hide");
        onPageLoading();
      },
      error: function (err) {
        toastr.error("Đã có lỗi xảy ra trong quá trình xóa");
        console.log(err.status);
      },
    });
  }
}
