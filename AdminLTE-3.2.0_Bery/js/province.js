// REGION 1
var gTable = $("#example1")
  .DataTable({
    responsive: true,
    lengthChange: false,
    autoWidth: false,
    buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
    columns: [
      {
        data: null,
        render: function (data, type, row) {
          return '<a title="Xem" class="view" style="cursor: pointer;"><i class="fas fa-search"></i></a>';
        },
      },
      {
        data: null,
        render: function (data, type, row) {
          return '<a title="Sửa" class="edit" style="cursor: pointer"><i class="fas fa-edit"></i></a>';
        },
      },
      {
        data: null,
        render: function (data, type, row) {
          return '<a title="Xóa" class="delete" style="cursor: pointer"><i class="fas fa-trash"></i></a>';
        },
      },
      {
        data: null,
        render: function (data, type, row) {
          return '<button class="btn btn-info btn-sm info">Thông tin BDS</button>';
        },
      },
      { data: "id" },
      { data: "name" },
      { data: "code" },
    ],
  })
  .buttons()
  .container()
  .appendTo("#example1_wrapper .col-md-6:eq(0)");
// REGION 2
onPageLoading();
// Sự kiện click cho nút "Thêm"
$("#btn-add-province").on("click", function () {
  $("#modal-add").modal("show");
});
// Sự kiện click cho nút "add" (modal-add)
$("#btn-modalAdd-save").on("click", function () {
  onBtnModalAddSaveClick();
});
// Sự kiện click cho nút "Xem"
$("#example1").on("click", ".view", function () {
  var vTable = $("#example1").DataTable();
  var rowData = vTable.row($(this).closest("tr")).data();
  $("#modal-view").modal("show");
  $("#view-province-id").html(rowData.id);
  $("#view-province-name").html(rowData.name);
  $("#view-province-code").html(rowData.code);
});
// Sự kiện click cho nút trên modal-view
$("#modal-view").on("click", ".ew-action", function () {
  var action = $(this).data("action");
  var vEditId = $("#view-province-id").html();
  var vEditName = $("#view-province-name").html();
  var vEditCode = $("#view-province-code").html();
  if (action === "add") {
    onBtnAddModalViewClick();
  }
  if (action === "edit") {
    onBtnEditModalViewClick(vEditId, vEditName, vEditCode);
  }
  if (action === "delete") {
    onBtnDeleteModalViewClick(vEditId, vEditName, vEditCode);
  }
});
// Sự kiện click cho nút "Sửa"
$("#example1").on("click", ".edit", function () {
  var vTable = $("#example1").DataTable();
  var rowData = vTable.row($(this).closest("tr")).data();
  $("#modal-edit").modal("show");
  $("#inp-edit-idProvince").val(rowData.id);
  $("#inp-edit-nameProvince").val(rowData.name);
  $("#inp-edit-codeProvince").val(rowData.code);
});
// Sự kiện click cho nút save trên modal-edit
$("#btn-modalEdit-save").on("click", function () {
  var rowDataId = $("#inp-edit-idProvince").val();
  callApiPutProvinceById(rowDataId);
});
// Sự kiện click cho nút "Xóa"
$("#example1").on("click", ".delete", function () {
  var vTable = $("#example1").DataTable();
  var rowData = vTable.row($(this).closest("tr")).data();
  $("#modal-delete").modal("show");
  $("#el1_province_id").html(rowData.id);
  $("#el1_province__name").html(rowData.name);
  $("#el1_province__code").html(rowData.code);
});
// Sự kiện click cho nút delete trên modal delete
$("#btn-confirm-modalDelete").on("click", function () {
  var vId = $("#el1_province_id").html();
  callApiDeleteProvince(vId);
});
// Sự kiện click cho nút "Thông tin bds"
$("#example1").on("click", ".info", function () {
  var vTable = $("#example1").DataTable();
  var rowData = vTable.row($(this).closest("tr")).data();
  console.log("thông tin BDS: " + rowData.id);
});
// Sự kiện hover vào các nút action trên modal view
$('[data-toggle="tooltip"]').tooltip();
// REGION 3
// Hàm thực hiện tải trang
function onPageLoading() {
  callApiGetAllProvince();
}
// Hàm thực hiện sự kiện nút save trên modal add được click
function onBtnModalAddSaveClick() {
  var vProvinceObj = {
    name: $("#inp-name-province").val().trim(),
    code: $("#inp-code-province").val().trim(),
  };
  // Kiêm tra dữ liệu đầu vào
  var isCheck = validationModalAdd(vProvinceObj);
  if (isCheck) {
    callApiPostProvince(vProvinceObj);
  }
}
// Hàm thực hiện sự kiện nút add trên modal-view được click
function onBtnAddModalViewClick() {
  $("#modal-view").modal("hide");
  setTimeout(function () {
    $("#modal-add").modal("show");
  }, 250);
}
// Hàm thực hiện sự kiện nút edit trên modal-view được click
function onBtnEditModalViewClick(paramId, paramName, paramCode) {
  $("#modal-view").modal("hide");
  setTimeout(function () {
    $("#modal-edit").modal("show");
    $("#inp-edit-idProvince").val(paramId);
    $("#inp-edit-nameProvince").val(paramName);
    $("#inp-edit-codeProvince").val(paramCode);
  }, 250);
}
// Hàm thực hiện sự kiện nút delete trên modal-view được click
function onBtnDeleteModalViewClick(paramId, paramName, paramCode) {
  $("#modal-view").modal("hide");
  setTimeout(function () {
    $("#modal-delete").modal("show");
    $("#el1_province_id").html(paramId);
    $("#el1_province__name").html(paramName);
    $("#el1_province__code").html(paramCode);
  }, 250);
}
// REGION 4
//  Hàm gọi api lấy tất cả danh sách tỉnh thành
function callApiGetAllProvince() {
  $.ajax({
    url: "http://localhost:8080/province",
    type: "GET",
    success: function (res) {
      loadDataToTable(res);
    },
    error: function (err) {
      toastr.error("Đã có lỗi xẫy ra");
      console.log(err.status);
    },
  });
}
// Truyền dữ liệu vào trong table
function loadDataToTable(paramData) {
  var vTable = $("#example1").DataTable();
  vTable.clear().rows.add(paramData).draw();
}
// Hàm thẩm định đầu vào trên modal add
function validationModalAdd(paramObj) {
  if (paramObj.name.trim() === "") {
    // Hiển thị thông báo Toast nếu "name" không được nhập
    toastr.warning("vui lòng nhập tên tỉnh thành");
    return false;
  }
  if (paramObj.code.trim() === "") {
    toastr.warning("vui lòng nhập mã tỉnh thành");
    return false;
  }
  return true;
}
// Hàm gọi api để thêm tỉnh thành
function callApiPostProvince(paramObj) {
  $.ajax({
    url: "http://localhost:8080/province",
    type: "POST",
    data: JSON.stringify(paramObj),
    contentType: "application/json",
    success: function (res) {
      toastr.success("Thêm thành công");
      $("#inp-name-province").val("");
      $("#inp-code-province").val("");
      $("#modal-add").modal("hide");
      onPageLoading();
    },
    error: function (err) {
      toastr.error("Đã có lỗi xẫy ra trong quá trình tạo mới");
      console.log(err.status);
    },
  });
}
// Hàm gọi api để update tỉnh thành
function callApiPutProvinceById(paramId) {
  var vProvinceObj = {
    name: $("#inp-edit-nameProvince").val(),
    code: $("#inp-edit-codeProvince").val(),
  };
  $.ajax({
    url: "http://localhost:8080/province/" + paramId,
    type: "PUT",
    data: JSON.stringify(vProvinceObj),
    contentType: "application/json",
    success: function (res) {
      toastr.success("Cập nhật thành công");
      $("#modal-edit").modal("hide");
      onPageLoading();
    },
    error: function (err) {
      toastr.error("Đã có lỗi xẫy ra trong quá trình cập nhật");
      console.log(err.status);
    },
  });
}
// Hàm gọi api delete tỉnh thành
function callApiDeleteProvince(paramId) {
  // Hiển thị xác nhận từ người dùng trước khi xóa
  if (confirm("Bạn có chắc chắn muốn xóa tỉnh thành này không?")) {
    $.ajax({
      url: "http://localhost:8080/province/" + paramId,
      type: "DELETE",
      success: function (res) {
        toastr.success("Xóa thành công");
        $("#modal-delete").modal("hide");
        onPageLoading();
      },
      error: function (err) {
        toastr.error("Đã có lỗi xảy ra trong quá trình xóa");
        console.log(err.status);
      },
    });
  }
}
